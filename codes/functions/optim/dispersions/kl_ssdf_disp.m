function disp = kl_ssdf_disp(m, mdl)
    disp = mdl.Tinv * sum( kl_div(m, 1.0) );
end
