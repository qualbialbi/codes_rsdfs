function ssdf = hj_link(opt_w, mdl)
    ssdf = max(-mdl.returns * opt_w, 0.0);
end
