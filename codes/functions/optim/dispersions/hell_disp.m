function disp = hell_disp(w, mdl)
    disp = sum( inv_pos( ones(mdl.T, 1) + mdl.returns * w ) ) * mdl.Tinv + sum(w);
end
