function disp = hjn_disp(w, mdl)
    disp = sum( (mdl.returns * w).^2 ) * 0.5 * mdl.Tinv + sum(w);
end
