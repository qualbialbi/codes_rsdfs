function disp = kl_disp(w, mdl)
    disp = sum( exp(-mdl.returns * w) - ones(mdl.T, 1) ) * mdl.Tinv + sum(w);
end
