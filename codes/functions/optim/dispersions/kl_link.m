function ssdf = kl_link(opt_w, mdl)
    ssdf = exp(-mdl.returns * opt_w);
end
