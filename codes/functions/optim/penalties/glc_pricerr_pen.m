function err = glasso_pricerr_pen(dpricerr, mdl)
    tmp = [];
    for g = 1:mdl.dgroups(end)
        tmp = [tmp, norm( mdl.weight_mat_inv{g} * dpricerr(mdl.dgroups == g), 2)];
    end
    pen = max(pen);
end
