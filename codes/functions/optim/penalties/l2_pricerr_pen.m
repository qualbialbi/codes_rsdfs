function err = l2_pricerr_pen(dpricerr, mdl)
    err = norm(dpricerr, 2);
end
