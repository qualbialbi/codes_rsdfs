function pen = elnet_pen(wd, mdl)
 
        cvx_begin quiet
            variable z(mdl.Nd)
            minimize( sum_square_abs(z - wd) )
            subject to
                norm(z, Inf) <= mdl.tau;
        cvx_end
 
    pen = 0.5 * (1.0 / mdl.alpha) * cvx_optval;
end
