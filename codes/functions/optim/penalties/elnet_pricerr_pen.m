function err = elnet_pricerr_pen(dpricerr, mdl)
    err = mdl.tau * norm(dpricerr, 1) + 0.5 * mdl.alpha * sum( dpricerr.^2 );
end
