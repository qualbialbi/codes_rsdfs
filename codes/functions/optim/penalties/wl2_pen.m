function pen = wl2_pen(wd, mdl)
    pen = mdl.tau * norm(mdl.weight_mat * wd, 2);
end
