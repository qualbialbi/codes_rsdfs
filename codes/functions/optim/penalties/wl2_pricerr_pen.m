function err = wl2_pricerr_pen(dpricerr, mdl)
    err = norm(mdl.weight_mat_inv * dpricerr, 2);
end
