function pen = l2_pen(wd, mdl)
    pen = mdl.tau * norm(wd, 2);
end
