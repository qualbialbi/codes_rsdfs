function mdl = model_setup(mdl)

    switch mdl.disp
    case 'hj'
        mdl.disp_fun  = @hj_disp;
        mdl.link_fun = @hj_link;
        mdl.ssdf_disp_fun = @hj_ssdf_disp;
    case 'hjn'
        mdl.disp_fun  = @hjn_disp;
        mdl.link_fun = @hjn_link;
        mdl.ssdf_disp_fun = @hjn_ssdf_disp;
    case 'kl'
        mdl.disp_fun  = @kl_disp;
        mdl.link_fun = @kl_link;
        mdl.ssdf_disp_fun = @kl_ssdf_disp;
    case 'ne'
        mdl.disp_fun  = @negen_disp;
        mdl.link_fun = @negen_link;
        mdl.ssdf_disp_fun = @negen_ssdf_disp;
    case 'he'
        mdl.disp_fun  = @hell_disp;
        mdl.link_fun = @hell_link;
        mdl.ssdf_disp_fun = @hell_ssdf_disp;
    end

    switch mdl.pen
    case 'l2'
        mdl.pen_fun = @l2_pen;
        mdl.pricerr_fun = @l2_pricerr_pen;
    case 'wl2'
        mdl.pen_fun = @wl2_pen;
        mdl.pricerr_fun = @wl2_pricerr_pen;
    case 'glc'
        mdl.pen_fun = @glc_pen;
        mdl.pricerr_fun = @glc_pricerr_pen;
    case 'el'
        mdl.pen_fun = @elnet_pen;
        mdl.pricerr_fun = @elnet_pricerr_pen;
    end
        

end
