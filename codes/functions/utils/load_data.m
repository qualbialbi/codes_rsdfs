function mdl = load_data(mdl)

    try
        load(mdl.fpath);
    catch
        error('provide valid fpath: path to .mat file constaining data');
    end

    mdl.dates = dates;
    mdl.T = numel(dates);
    mdl.Tinv = 1.0 / mdl.T;
    mdl.returns = returns;
    mdl.N = size(returns, 2);
    mdl.Nd = mdl.N - mdl.Ns;

end
