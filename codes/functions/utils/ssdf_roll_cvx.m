function [opt_w, ssdf, ssdf_disp, pricerr, dpricerr_size, is_ssdf] = ssdf_roll_cvx(mdl)
    
    ntaus = numel(mdl.taus);
    nwindows = mdl.T - mdl.roll_window + 1;
    ssdf_disp = zeros(nwindows, ntaus);
    ssdf = zeros(mdl.roll_window, ntaus);
    pricerr = zeros(mdl.N, nwindows, ntaus);
    opt_w = zeros(mdl.N, nwindows, ntaus);
    dpricerr_size = zeros(nwindows, ntaus);
    is_ssdf = true(nwindows, ntaus);

    if strcmp(mdl.pen, 'nopen')
        obj_fun = mdl.disp_fun;
    else
        obj_fun = @(w, mdl) mdl.disp_fun(w, mdl) + mdl.pen_fun( w( mdl.Ns+1:end ), mdl);
    end

    for ww = 1:nwindows
        mdl1 = mdl;
        mdl1.dates_idx = [false(1, ww-1), true(1, mdl.roll_window), false(1, nwindows - ww)];
        mdl1 = update_data(mdl1);
        
        for t = 1:ntaus

            mdl1.tau = mdl1.taus(t);
            cvx_begin quiet
                variable w(mdl.N)
                minimize( obj_fun(w, mdl1) )
            cvx_end
 
            opt_w(:,ww,t) = w;
            ssdf(:,ww,t) = mdl1.link_fun(w, mdl1);
            ssdf_disp(ww,t) = mdl1.ssdf_disp_fun( ssdf(:,ww,t), mdl1 );
            pricerr(:,ww,t) = ones(mdl1.N, 1) - mdl1.returns.' * ssdf(:,ww,t) * mdl1.Tinv;
            dpricerr_size(ww,t) = mdl1.pricerr_fun( pricerr( mdl1.Ns+1:end, ww, t ), mdl1 );
            spricerr = abs( pricerr( 1:mdl1.Ns, ww, t ) );
 
            if dpricerr_size(ww,t) > mdl1.tau+1e-4 || any( spricerr > 1e-4 )
                is_ssdf(ww,t) = false;
            end
        end        
    end
end
