function [opt_w, ssdf, ssdf_disp, pricerr, dpricerr_size, is_ssdf] = cvx_run(mdl)
    
    ntaus = numel(mdl.taus);
    ssdf_disp = zeros(1, ntaus);
    ssdf = zeros(mdl.T, ntaus);
    pricerr = zeros(mdl.N, ntaus);
    opt_w = zeros(mdl.N, ntaus);
    dpricerr_size = zeros(1, ntaus);
    is_ssdf = true(1, ntaus);
    
    if strcmp(mdl.pen, 'nopen')
        obj_fun = mdl.disp_fun;
    else
        obj_fun = @(w, mdl) mdl.disp_fun(w, mdl) + mdl.pen_fun( w( mdl.Ns+1:end ), mdl);
    end

    for t = 1:ntaus

        mdl.tau = mdl.taus(t);
        cvx_begin quiet
            variable w(mdl.N)
            minimize( obj_fun(w, mdl) )
        cvx_end

        opt_w(:,t) = w;
        ssdf(:,t) = mdl.link_fun(w, mdl);
        ssdf_disp(t) = mdl.ssdf_disp_fun( ssdf(:,t), mdl );
        pricerr(:,t) = ones(mdl.N, 1) - mdl.returns.' * ssdf(:,t) * mdl.Tinv;
        dpricerr_size(t) = mdl.pricerr_fun( pricerr( mdl.Ns+1:end, t ), mdl );
        spricerr = abs( pricerr( 1:mdl.Ns, t ) );

        if dpricerr_size(t) > mdl.tau+1e-4 || any( spricerr > 1e-4 )
            is_ssdf(t) = false;
        end
    end

end
