function mdl = ssdf_setup(mdl)
    mdl = default_setup(mdl);
    mdl = model_setup(mdl);
end
