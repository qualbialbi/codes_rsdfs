function res = ssdf_run(mdl)
    if isfield(mdl, 'returns_idx') || isfield(mdl, 'dates_idx')
        mdl = update_data(mdl);
    end

    if isfield(mdl, 'roll_window')
        
        if mdl.roll_window <= 0 || mdl.roll_window >= mdl.T
            error('mdl.roll_window must be between 0 and mdl.T');
        end
        [res.opt_w, res.ssdf, res.ssdf_disp, res.pricerr, res.dpricerr_size, res.is_ssdf] = ssdf_roll_cvx(mdl);

    else
    
        [res.opt_w, res.ssdf, res.ssdf_disp, res.pricerr, res.dpricerr_size, res.is_ssdf] = ssdf_cvx(mdl);
    
    end
end
