function mdl = update_data(mdl)
    if ~isfield(mdl, 'returns_idx')
        mdl.returns_idx = true(1, mdl.N);
    elseif ~islogical(mdl.returns_idx) || numel(mdl.returns_idx) ~= mdl.N
        error('mdl.returns_idx must be logical of length mdl.N');
    end

    if ~isfield(mdl, 'dates_idx')
        mdl.dates_idx = true(1, mdl.T);
    elseif ~islogical(mdl.dates_idx) || numel(mdl.dates_idx) ~= mdl.T
        error('mdl.dates_idx must be logical of length mdl.T')
    end

    mdl.dates = mdl.dates(mdl.dates_idx);
    mdl.T = numel(mdl.dates);
    mdl.Tinv = 1.0 / mdl.T;
    mdl.returns = mdl.returns(mdl.dates_idx, mdl.returns_idx);
    mdl.N = size(mdl.returns, 2);
    mdl.Nd = mdl.N - mdl.Ns;

end
