function mdl = default_setup(mdl)

    % fpath file path
    if ~isfield(mdl, 'fpath')
        error('provide mdl.fpath');
    elseif ~ischar(mdl.fpath)
        error('mdl.fpath must be of type char');
    end

    % Ns number sure assets
    if ~isfield(mdl, 'Ns')
        error('mdl.Ns must be provided');
    elseif ~isnumeric(mdl.Ns)
        error('mdl.Ns must be numeric');
    end

    % load data
    mdl = load_data(mdl);

    if ~isfield(mdl, 'disp')
        error('mdl.disp must be provided');
    elseif ~any( strcmp(mdl.disp, {'hj','kl','ne','he','rb'}) )
        error('mdl.disp must be a valid divergence');
    end

    if mdl.Ns == mdl.N
        mdl.pen = 'nopen';
    elseif ~isfield(mdl, 'pen')
        error('mdl.pen must be provided');
    elseif ~any( strcmp(mdl.pen, {'l2','wl2','glc', 'el'}) )
        error('mdl.pen must be a valid penalty');
    end

    if ~strcmp(mdl.pen, 'nopen') & ~isfield(mdl, 'taus')
        error('mdl.taus must be provided');
    elseif ~isnumeric(mdl.taus)
        error('mdl.taus must be numeric');
    end

end
