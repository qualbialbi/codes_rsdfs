%% test ssdf setup

paths_setup();
%load cvx
cvx_setup;

%% SETUP
% path to sure and dubious assets
% - fpath   : (char vector) path to .mat file containing a 1XT
%             datetime vector saved as `dates` and a TxN matrix of
%             returns saved as `returns` on working directory

%mdl.fpath = 'FF3F100P_monthly.mat';
mdl.fpath = 'WFRmonthlywmarket.mat';
%mdl.fpath= 'WFRmonthlywmarket_norf.mat';
%mdl.fpath = 'WFRmonthly.mat';

% number of sure assets
% - mdl.Ns      : (double) [optional] number of sure assets
%                 [default = 0]
mdl.Ns = 2;

% model primal divergence
% - mdl.disp     : (char) [optional] choice of dispersion function in 
%                  the primal
%                 - 'hj' : Hansen-Jagannathan divergence [default]
%                 - 'kl' : Kullback-Leiber divergence
%                 - 'hjn' : Hansen-Jagannathan divergence also negative
%                 - 'ne' : Negative entropy
%                 - 'he' : Hellingher
mdl.disp = 'hj';

% pricing error penalty
% - mdl.pen     : (char) [optional] choice of penalty function in the 
%                 primal [default = false]
%                 - 'l2' : l2
%                 - 'wl2' : weighted l2
%                 - 'glc' : group lasso conjuagte
%                 - 'el' : elastic-net
mdl.pen = 'el';

% model tau values
% - mdl.taus : (double vector) [optional ]values of constraint 
%                  parameter [default = 1]

mdl.taus = [8 250]; 
mdl.alpha = 2;

% setup mdl

mdl = ssdf_setup(mdl);
% OUTPUT
% - mdl : (struct) containing model info

%% RUN
res = ssdf_run(mdl);

%% RUN rolling
mdl.roll_window = 12 * 30;
res_roll = ssdf_run(mdl);
