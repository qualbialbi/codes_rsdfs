function paths_setup()
 
    % add paths
    addpath(genpath('../../codes'));
    addpath(genpath('../../data'));
 
end
